import requests


from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    # Use the Pexels API
    # Create a dictionary for the headers to use in the request
    query = f"{city}, {state}"
    url = f"https://api.pexels.com/v1/search?query={query}"
    # Create the URL for the request with the city and state
    # Make the request
    response = requests.get(url, headers=headers)
    # Parse the JSON response
    picture_url = response.json()["photos"][0]["src"]["original"]
    # Get the photo URL

    # Parse the JSON response
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    return {"picture_url": picture_url}


def get_lat_lon(location):
    params = {
        "q": f"{location.city}, {location.state.abbreviation},USA",
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params)

    lat = response.json()[0]["lat"]
    lon = response.json()[0]["lon"]
    print(lat, lon)
    return {"lat": lat, "lon": lon}


def get_weather_data(location):
    # Use the Open Weather API
    # headers = {"Authorization": OPEN_WEATHER_API_KEY}
    # Create the URL for the geocoding API with the city and state

    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response
    lat_lon = get_lat_lon(location)
    if lat_lon is None:
        return None
    params = {
        "lat": lat_lon["lat"],
        "lon": lat_lon["lon"],
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    url = "https://api.openweathermap.org/data/2.5/weather?"
    # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    # Parse the JSON response
    response = requests.get(url, params)
    # Get the main temperature and the weather's description and put
    description = response.json()["weather"][0]["description"]
    temp = response.json()["main"]["temp"]
    #   them in a dictionary
    # Return the dictionary
    return {"description": description, "temp": temp}
