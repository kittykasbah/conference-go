# http://api.openweathermap.org/geo/1.0/direct {'lat': 41.8755616, 'lon': -87.6244212, 'appid': '0ed0adf9f7887e2ced0eb237a4be3a3', 'units': 'imperial'}

url = "http://api.openweathermap.org/data/2.5/weather"
# params = {'lat': 41.8755616, 'lon': -87.6244212, 'appid': '0ed0adf9f7887e2ced0eb237a4be3a3', 'units': 'imperial'}
params = {
    "lat": 41.8755616,
    "lon": -87.6244212,
    "appid": "0ed0adf9f70887e2ced0eb237a4be3a3",
    "units": "imperial",
}

import requests

response = requests.get(url, params)
# curl https://api.openweathermap.org/data/2.5/weather?appid=0ed0adf9f70887e2ced0eb237a4be3a3&lat=41.875&lon=-87.624
